class AddCoverToPost < ActiveRecord::Migration
  def self.up
    add_attachment :posts, :cover
  end
  def self.down
    remove_attachment :posts, :cover
  end
end
