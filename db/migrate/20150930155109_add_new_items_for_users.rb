class AddNewItemsForUsers < ActiveRecord::Migration
  def change
    add_column :users, :facebook, :string
    add_column :users, :twitter,  :string
    add_column :users, :instagram, :string
    add_column :users, :git, :string
    add_column :users, :linkedin, :string
    add_column :users, :about, :text
  end
end
