Rails.application.routes.draw do
  get 'page/index' => 'page#index'

  devise_for :users, :controllers => { registrations: 'users/registrations'}
  resources :posts, :path => 'entradas'
  resources :categories, :path => 'categorias'
  resources :images, :path => 'imagenes'
  root "posts#index"
end
