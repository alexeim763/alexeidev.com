class PostsController < ApplicationController
  before_action :find_post, only: [:show,:edit, :destroy , :update]
  before_action :authenticate_user!, only: [:new, :create, :edit, :destroy, :update]
  before_action :no_index, only: [:new, :edit]
  before_action :og_tags, only: [:show]
	def index
    @page_title = 'Blog de Tecnología y Desarrollo'
    @posts = Post.all.order("created_at desc").paginate(:page => params[:page], :per_page => 5)
	end

  def new
    @post = Post.new
    @categories = Category.all
  end

  def create
    @post = Post.new post_params
    @post.user = current_user
    if @post.save
      redirect_to posts_path, notice: "Bien, se creó el Post"
    else
      render 'new', notice: "Opps, Ocurrio algun error revisa"
    end
  end

  def show
    @page_title = @post.title
  end
  def update
    params[:post][:category_ids] ||= []
    if @post.update post_params
      redirect_to @post, notice: 'Lo logramos, si se pudo actualizar'
    else
      redirect_to 'edit', notice: 'Algo salio Mál'
    end
  end
  def edit
    @categories = Category.all
  end

  def destroy
    @post.destroy
    redirect_to posts_path
  end

  private
  def post_params
    params.require(:post).permit(:title,:content, :user, :cover, { category_ids:[] })
  end
  def find_post
    @post = Post.friendly.find(params[:id])
  end
  def no_index
    set_meta_tags noindex: true
  end
  def og_tags

    set_meta_tags og: {
      title:    @post.title,
      type:     'article',
      url:      post_url(@post),
      image:    URI.join(request.url, @post.cover.url(:thumbnail))
    }
  end
end

