class ImagesController < ApplicationController
  before_action :find_image, only: [:show, :edit, :destroy, :update]
  before_action :authenticate_user!, only: [:new, :create, :edit, :destroy, :update]
  before_action :no_index
  def index
    @images = Image.all
  end
  def new 
    @image = Image.new
  end
  def create
    @image = Image.new image_params
    if @image.save
      redirect_to images_path, notice: "Bien se subio la imagen"
    else
      render 'new', notice: "Opps, ocurrio algun error"
    end
  end
  private
  def image_params
    params.require(:image).permit(:name,:image)
  end
  def find_image
    @image = Image.find(params[:id])
  end
  def no_index
    set_meta_tags noindex: true
  end
end
