class CategoriesController < ApplicationController
  before_action :find_category, only: [:show, :edit, :update , :destroy]
  before_action :authenticate_user!, only: [:index, :new, :edit, :update , :destroy]
  before_action :no_index, only: [:index, :new, :edit]
  def index
    @categories = Category.all
    @page_title = 'Categorias'
  end
  def new
    @category = Category.new
  end
  def create
    @category = Category.new category_params
    if @category.save
      redirect_to categories_path, notice: "Bien, creaste una categoria"
    else
      render 'new', notice: "Opps, Ocurrio algun tipo de error"
    end
  end

  def edit
  end

  def show
    @posts = @category.posts.order("created_at desc").paginate(:page => params[:page], :per_page => 3)
    @page_title = @category.name
  end

  def update
    if @category.update category_params
      redirect_to categories_path, notice: 'Se actualizo correctamente'
    else
      render 'new', notice: "Opps, Ocurrio algun tipo de error"
    end
  end
  def destroy
    @category.destroy
    redirect_to categories_path
  end
  private
  def category_params
    params.require(:category).permit(:name, :color)
  end

  def find_category
    @category = Category.friendly.find(params[:id])
  end
  def no_index
    set_meta_tags noindex: true
  end

end
