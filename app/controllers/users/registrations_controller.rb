class Users::RegistrationsController < Devise::RegistrationsController

  private

  def sign_up_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation, :facebook, :twitter, :instagram , :git ,:linkedin, :about)

  end

  def account_update_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation, :current_password, :facebook, :twitter, :instagram , :git ,:linkedin, :about, :avatar)
  end
end
