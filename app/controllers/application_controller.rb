class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  before_action :prepare_meta_tags, if: "request.get?"

  protect_from_forgery with: :exception

  before_action :set_posts
  before_action :set_author
  
  def prepare_meta_tags(options={})

    site_name   = "AlexeiDev"
    site        = "AlexeiDev"
    title       = "AlexeiDev - Aprende lo ultimo en tecnología"
    description = "Aprende lo último en tecnología y desarrollo, Ruby on rails, Python, Javascript, Android, ECMA 2015, CSS, HTML5"
    current_url = request.url

    defaults = {
      site:        site_name,
      title:       title,
      description: description,
      keywords:    %w[web software desarrollo moviles aplicaciones rails python ],
      twitter:     {site_name: site_name,
                    site: '@alexeidev',
                    card: 'resumen',
                    description: description
                    },
      og:          {url: current_url,
                    site_name: site_name,
                    title: title,
                    image: image_url('logo.png'),
                    description: description,
                    type: 'website'}    
    }
    options.reverse_merge!(defaults)
    set_meta_tags options
  end

  private

  def set_posts
    @postFeatured = Post.last(3)
  end
  def set_author
    @widgetAuthor = User.first
  end

end
