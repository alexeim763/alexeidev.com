class Post < ActiveRecord::Base
  validates :title, :content , presence: true
  belongs_to :user
  has_and_belongs_to_many :categories  
  extend FriendlyId
  friendly_id :title, use: :slugged
  has_attached_file :cover, styles: { medium: "500", thumbnail: "200" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :cover, content_type: /\Aimage\/.*\Z/
end
