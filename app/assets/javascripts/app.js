var ready = function() { 
  var $ = window.jQuery
  var is_open = false;
  var openButton = $('.MainHeader-iconMenu');
  var closeButton = $('.NavCanvas-buttonClose');
  var menu = $('.NavCanvas');

  openButton.on('click', toggleNav );
  if( closeButton ){
      closeButton.on('click', toggleNav);
  }

  menu.on('click', function (ev){
      var target = ev.target;
      if(is_open && target !== openButton){
          toggleNav();
      }
  })


  function toggleNav() {
      if (is_open) {
          $('.NavCanvas').removeClass('NavCanvas-showNav');
          $('.NavCanvas-list').removeClass('NavCanvas-showNav');
      } else {
          $('.NavCanvas').addClass('NavCanvas-showNav');
          $('.NavCanvas-list').addClass('NavCanvas-showNav');
      }
      is_open = !is_open;
  }

  new WOW().init();
}
$(document).ready(ready)
$(window).bind('page:change', ready)
